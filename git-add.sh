#!/bin/bash -e

# Note: This does not escape filenames. Which means this will not correctly track filenames
# with whitespace, [, ], or other similar characters which have special meaning as a git
# pattern, which is what "git lfs track" in fact expects. In this case, for those filenames,
# they filenames should be manually escaped.

find * -type f -size +100k -exec git lfs track '{}' +
