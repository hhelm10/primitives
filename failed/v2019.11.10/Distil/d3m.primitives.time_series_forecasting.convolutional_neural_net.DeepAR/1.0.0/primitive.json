{
    "id": "3410d709-0a13-4187-a1cb-159dd24b584b",
    "version": "1.0.0",
    "name": "DeepAR",
    "keywords": [
        "time series",
        "forecasting",
        "convolutional neural network",
        "autoregressive"
    ],
    "source": {
        "name": "Distil",
        "contact": "mailto:jeffrey.gleason@yonder.co",
        "uris": [
            "https://github.com/NewKnowledge/TimeSeries-D3M-Wrappers"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/NewKnowledge/TimeSeries-D3M-Wrappers.git@a69c4c0bcdf9cd0b7998cc244da02b61f75d882d#egg=TimeSeriesD3MWrappers"
        }
    ],
    "python_path": "d3m.primitives.time_series_forecasting.convolutional_neural_net.DeepAR",
    "algorithm_types": [
        "CONVOLUTIONAL_NEURAL_NETWORK"
    ],
    "primitive_family": "TIME_SERIES_FORECASTING",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "TimeSeriesD3MWrappers.primitives.forecasting_deepar.DeepAR",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "TimeSeriesD3MWrappers.primitives.forecasting_deepar.Params",
            "Hyperparams": "TimeSeriesD3MWrappers.primitives.forecasting_deepar.Hyperparams"
        },
        "interfaces_version": "2019.11.10",
        "interfaces": [
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "emb_dim": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 64,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "number of cells to use in the categorical embedding component of the model",
                "lower": 8,
                "upper": 256,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "lstm_dim": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 64,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "number of cells to use in the lstm component of the model",
                "lower": 8,
                "upper": 256,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "epochs": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 100,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "number of training epochs",
                "lower": 1,
                "upper": 9223372036854775807,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "steps_per_epoch": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 20,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "number of steps to do per epoch",
                "lower": 5,
                "upper": 200,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "early_stopping_patience": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 5,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "number of epochs to wait before invoking early stopping criterion",
                "lower": 0,
                "upper": 9223372036854775807,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "early_stopping_delta": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 1,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "early stopping will interpret change of < delta in desired direction\n            will increment early stopping counter state",
                "lower": 0,
                "upper": 9223372036854775807,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "learning_rate": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.01,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "learning rate",
                "lower": 0.0,
                "upper": 1.0,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "batch_size": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 16,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "batch size",
                "lower": 1,
                "upper": 256,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "dropout_rate": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.1,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "dropout to use in lstm model (input and recurrent transform)",
                "lower": 0.0,
                "upper": 1.0,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "window_size": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 20,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "window size of sampled time series in training process",
                "lower": 10,
                "upper": 9223372036854775807,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "negative_obs": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 1,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "whether to sample time series with padded observations before t=0 in training",
                "lower": 0,
                "upper": 10,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "val_split": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.2,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "proportion of training records to set aside for validation. Ignored \n            if iterations flag in `fit` method is not None",
                "lower": 0.0,
                "upper": 1.0,
                "lower_inclusive": true,
                "upper_inclusive": false
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "TimeSeriesD3MWrappers.primitives.forecasting_deepar.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "TimeSeriesD3MWrappers.primitives.forecasting_deepar.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Fits DeepAR model using training data from set_training_data and hyperparameters\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs : Outputs\n    The outputs given to ``set_training_data``.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "TimeSeriesD3MWrappers.primitives.forecasting_deepar.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce primitive's predictions for specific time series at specific future time instances\n    * these specific timesteps / series are specified implicitly by input dataset\n\nParameters\n----------\ninputs : dataframe containing meta information about series and / or covariate (at each timestep)\n\nReturns\n-------\nOutputs: dataframe with predictions for specific time series at specific future time instances"
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets primitive's training data\n\nParameters\n----------\ninputs: dataframe containing meta information about series and / or covariate (at each timestep)\n\noutputs: dataframe / series containing target observations at specific timesteps"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {}
    },
    "structural_type": "TimeSeriesD3MWrappers.primitives.forecasting_deepar.DeepAR",
    "description": "Primitive that applies a deep autoregressive forecasting algorithm for time series\nprediction. The implementation is based off of this paper: https://arxiv.org/pdf/1704.04110.pdf\nand is implemented in AWS's Sagemaker interface.\n\nTraining inputs: 1) Feature dataframe, 2) Target dataframe\nOutputs: Dataframe with predictions for specific time series at specific future time instances\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "39501669cb192bb61855facb37b083eafed952e49351047e34b7e9f25ecaa508"
}
