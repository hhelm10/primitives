{
    "id": "002f9ad1-46e3-40f4-89ed-eeffbb3a102b",
    "version": "0.1.0",
    "name": "K-fold cross-validation timeseries dataset splits",
    "python_path": "d3m.primitives.evaluation.kfold_timeseries_split.Common",
    "source": {
        "name": "common-primitives",
        "contact": "mailto:nklabs@newknowledge.com",
        "uris": [
            "https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/kfold_split_timeseries.py",
            "https://gitlab.com/datadrivendiscovery/common-primitives.git"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://gitlab.com/datadrivendiscovery/common-primitives.git@48c0ebf7bb5b86ee6f00a38ae52546dc4c5b3be5#egg=common_primitives"
        }
    ],
    "algorithm_types": [
        "K_FOLD",
        "CROSS_VALIDATION",
        "DATA_SPLITTING"
    ],
    "primitive_family": "EVALUATION",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "common_primitives.kfold_split_timeseries.KFoldTimeSeriesSplitPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Hyperparams": "common_primitives.kfold_split_timeseries.Hyperparams",
            "Params": "common_primitives.base.TabularSplitPrimitiveParams",
            "Outputs": "d3m.container.list.List",
            "Inputs": "d3m.container.list.List"
        },
        "interfaces_version": "2019.6.7",
        "interfaces": [
            "generator.GeneratorPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "number_of_folds": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 5,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Number of folds for k-folds cross-validation.",
                "lower": 2,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "number_of_window_folds": {
                "type": "d3m.metadata.hyperparams.Union",
                "default": null,
                "structural_type": "typing.Union[NoneType, int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Maximum size for a single training set.",
                "configuration": {
                    "fixed": {
                        "type": "d3m.metadata.hyperparams.Bounded",
                        "default": 1,
                        "structural_type": "int",
                        "semantic_types": [],
                        "description": "Number of folds in train set (window). These folds come directly before test set (streaming window).",
                        "lower": 1,
                        "upper": null,
                        "lower_inclusive": true,
                        "upper_inclusive": false
                    },
                    "all_records": {
                        "type": "d3m.metadata.hyperparams.Constant",
                        "default": null,
                        "structural_type": "NoneType",
                        "semantic_types": [],
                        "description": "Number of folds in train set (window) = maximum number possible."
                    }
                }
            },
            "time_column_index": {
                "type": "d3m.metadata.hyperparams.Union",
                "default": null,
                "structural_type": "typing.Union[NoneType, int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Column index to use as datetime index. If None, it is required that only one column with time column role semantic type is present and otherwise an exception is raised. If column index specified is not a datetime column an exception isalso be raised.",
                "configuration": {
                    "fixed": {
                        "type": "d3m.metadata.hyperparams.Bounded",
                        "default": 1,
                        "structural_type": "int",
                        "semantic_types": [],
                        "description": "Specific column that contains the time index",
                        "lower": 1,
                        "upper": null,
                        "lower_inclusive": true,
                        "upper_inclusive": false
                    },
                    "one_column": {
                        "type": "d3m.metadata.hyperparams.Constant",
                        "default": null,
                        "structural_type": "NoneType",
                        "semantic_types": [],
                        "description": "Only one column contains a time index. It is detected automatically using semantic types."
                    }
                }
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "common_primitives.kfold_split_timeseries.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.list.List",
                "kind": "PIPELINE"
            },
            "dataset": {
                "type": "d3m.container.dataset.Dataset",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "common_primitives.base.TabularSplitPrimitiveParams",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {
            "can_accept": {
                "arguments": {
                    "method_name": {
                        "type": "str"
                    },
                    "arguments": {
                        "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
                    },
                    "hyperparams": {
                        "type": "common_primitives.kfold_split_timeseries.Hyperparams"
                    }
                },
                "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]",
                "description": "Returns a metadata object describing the output of a call of ``method_name`` method under\n``hyperparams`` with primitive arguments ``arguments``, if such arguments can be accepted by the method.\nOtherwise it returns ``None`` or raises an exception.\n\nDefault implementation checks structural types of ``arguments`` expected arguments' types\nand ignores ``hyperparams``.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept numpy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nPrimitive arguments are a superset of method arguments. This method receives primitive arguments and\nnot just method arguments so that it is possible to implement it without a state between calls\nto ``can_accept`` for multiple methods. For example, a call to ``fit`` could during normal execution\ninfluences what a later ``produce`` call outputs. But during ``can_accept`` call we can directly have\naccess to arguments which would have been given to ``fit`` to produce metadata of the ``produce`` call.\n\nNot all primitive arguments have to be provided, only those used by ``fit``, ``set_training_data``,\nand produce methods, and those used by the ``method_name`` method itself.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\nhyperparams : Hyperparams\n    Hyper-parameters under which the method would be called during regular primitive execution.\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
            }
        },
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "This function computes everything in advance, including generating the relation graph.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "dataset",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : List\n    The inputs given to all produce methods.\noutputs : Outputs\n    The outputs given to ``set_training_data``.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "common_primitives.base.TabularSplitPrimitiveParams",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.list.List]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "For each input integer creates a ``Dataset`` split and produces the training ``Dataset`` object.\nThis ``Dataset`` object should then be used to fit (train) the pipeline.\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "produce_score_data": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.list.List]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "For each input integer creates a ``Dataset`` split and produces the scoring ``Dataset`` object.\nThis ``Dataset`` object should then be used to test the pipeline and score the results.\n\nOutput ``Dataset`` objects do not have targets redacted and are not directly suitable for testing."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "dataset"
                ],
                "returns": "NoneType",
                "description": "Sets training data of this primitive, the ``Dataset`` to split.\n\nParameters\n----------\ndataset : Dataset\n    The dataset to split."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "dataset": "typing.Union[NoneType, d3m.container.dataset.Dataset]",
            "main_resource_id": "typing.Union[NoneType, str]",
            "splits": "typing.Union[NoneType, typing.List[typing.Tuple[numpy.ndarray, numpy.ndarray]]]",
            "graph": "typing.Union[NoneType, typing.Dict[str, typing.List[typing.Tuple[str, bool, int, int, typing.Dict]]]]"
        }
    },
    "structural_type": "common_primitives.kfold_split_timeseries.KFoldTimeSeriesSplitPrimitive",
    "description": "A primitive which splits a tabular time-series Dataset for k-fold cross-validation.\n\nPrimitive sorts the time column so care should be taken to assure sorting of a\ncolumn is reasonable. E.g., if column is not numeric but of string structural type,\nstrings should be formatted so that sorting by them also sorts by time.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "8d87f11c966d33cc833de6910bf43a98b05882bca225cf2c24e56447539e8ad1"
}
